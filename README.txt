Atom Tags Topics Feature

Feature Dependencies:

Modules Used:
facetapi - http://drupal.org/project/facetapi

Description:
The Atom Tags Topics Features creates the tags and topics taxonomy vocabularies for use on various node types available in other Features. The Feature also enables Apache Solr facets for tags and topics and enables those facet blocks on the site search form.

Caveats:

